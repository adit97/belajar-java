package storage;

import java.util.Scanner;

public class trycatch {

	private static Scanner scan;

	public static void main(String[] args) {
		scan = new Scanner(System.in);
		System.out.print("input x : ");
		
		try {

			int x = scan.nextInt();
			System.out.println(x);
		}catch(Exception ex) {			
			ex.printStackTrace();
		}finally{
			System.out.println("prossed");
		}
		
		System.out.println("end file");
	}

}
