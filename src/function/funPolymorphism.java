package function;

public class funPolymorphism {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(new funPolymorphism().div(10, 5));
		System.out.println(new funPolymorphism().div(10, 5.0));
		System.out.println(new funPolymorphism().div(10.0, 5.0));

	}

	double div(int x, int y) {
		return x/y;
	}
	
	double div(int x, double y) {
		return x/y*2;
	}
	
	double div(double x, double y) {
		return x/y*3;
	}
	
}
