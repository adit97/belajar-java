package function;

public class funOverload {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(new funOverload().sum(2, 4));
		System.out.println(new funOverload().sum(2, 4, 6));
	}
	
	int sum(int x, int y) {
		return x+y;
	}
	
	int sum(int x, int y, int z) {
		return x+y+z;
	}
}
