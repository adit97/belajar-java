package sqlite;
import java.sql.*;
import java.util.Scanner;

public class connectToDb {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection c = null;
		Scanner sc;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:db.db");
			System.out.println("connect successfully");
			
			System.out.println("select service \n "
					+ "1- add \n "
					+ "2- select \n "
					+ "3- delete \n "
					+ "4- update \n");
			System.out.print("select : ");
			sc = new Scanner(System.in);
			int index = sc.nextInt();
			
			Statement s = c.createStatement();
			
			switch(index) {
				case 1:
					//add users
					Scanner user = new Scanner(System.in);
					System.out.print("input username : ");
					String username = user.nextLine();
					
					Scanner pass = new Scanner(System.in);
					System.out.print("input password : ");
					String password = pass.nextLine();
					
					String add = "insert into "
							+ "admins(username, password) "
							+ "values('"+username+"', '"+password+"')";
					
					s.executeUpdate(add);
					c.commit();
					s.close();
					c.close();
					System.out.println("data added");
					break;
				case 2:
					//select
					Statement s2 = c.createStatement();
					String select = "select * from admins";
					ResultSet rs = s2.executeQuery(select);
					
					System.out.println("id \t Username \t Password");
					
					while(rs.next()) {
						int id = rs.getInt("id");
						String un = rs.getString("username");
						String pw = rs.getString("password");
						System.out.println(id + "\t " + un + "\t \t " + pw);
					}
					rs.close();
					s2.close();
					c.close();	
					System.out.println("data selected");
					break;
				case 3:
					//delete
					Scanner idDel = new Scanner(System.in);
					System.out.print("input ID : ");
					int id = idDel.nextInt();
				
					String delete = "delete from admins where id="+id;
					s.executeQuery(delete);
					s.close();
					c.close();
					System.out.println("data deleted");
					break;
				case 4:
					//update password
					Scanner idUp = new Scanner(System.in);
					System.out.print("input Id : ");
					String idUpdate = idUp.nextLine();
					
					Scanner passUp = new Scanner(System.in);
					System.out.print("new password : ");
					String passUpdate = passUp.nextLine();
					
					String update = "update admins "
							+ "set password='"+passUpdate+"'"
							+ "where id="+idUpdate;
					
					s.executeUpdate(update);
					s.close();
					c.commit();
					c.close();
					System.out.println("data updated");
					break;
				default:
					break;
			}
			
		}catch(Exception e) {
			System.out.println("cannot connect");
			System.exit(0);
		}
	}

}
