package loop;

public class breakContinue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for(int x = 1; x <= 10; x++) {
			if(x == 5) {
				break;
			}
			System.out.println("number -> " + x);
		}
		
		System.out.println("---------------");	
		
		for(int x = 1; x <= 10; x++) {
			if(x == 5) {
				continue;
			}
			System.out.println("number -> " + x);
		}
	}

}
