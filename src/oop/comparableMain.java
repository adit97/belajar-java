package oop;

import java.util.ArrayList;
import java.util.Collections;

class Movie implements Comparable<Movie>{
	private double rate;
	private String name;
	private int year;
	
	public int compareTo(Movie m) {
		return this.year - m.year;
	}
	
	public Movie(String name, double rate, int year) {
		this.name = name;
		this.rate = rate;
		this.year = year;
	}
	
	public double getRate() { return rate;}
	public int getYear() { return year;}
	public String getName() { return name;}
}

public class comparableMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Movie> list = new ArrayList<Movie>();
		list.add(new Movie("Transformer", 8.8, 2014));
		list.add(new Movie("X men", 8.4, 2019));
		list.add(new Movie("Deadpool", 9.0, 2018));
		
		Collections.sort(list);
		
		for(Movie mov: list) {
			System.out.println(mov.getName() + " " + 
								mov.getRate() + " " + 
								mov.getYear());
		}
	}

}
