package oop;

public class genericClass<T> {

	private T t;
	
	public void set(T t) {
		this.t = t;
	}
	
	public T get() {
		return t;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		genericClass<Integer> intTest = new genericClass<Integer>();
		intTest.set(10);
		System.out.println(intTest.get());
		
		genericClass<String> stringTest = new genericClass<String>();
		stringTest.set("hai");
		System.out.println(stringTest.get());
	}

}
