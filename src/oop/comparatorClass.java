package oop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Movie1 implements Comparable<Movie1>{
	private double rate;
	private String name;
	private int year;
	
	public int compareTo(Movie1 m) {
		return this.year - m.year;
	}
	
	public Movie1(String name, double rate, int year) {
		this.name = name;
		this.rate = rate;
		this.year = year;
	}
	
	public double getRate() { return rate;}
	public int getYear() { return year;}
	public String getName() { return name;}
}

class RateCompare implements Comparator<Movie1>{
	public int compare(Movie1 m1, Movie1 m2) {
		if(m1.getRate() < m2.getRate()) return -1;
		if(m1.getRate() > m2.getRate()) return 1;
		else return 0;
	}
}

class NameCompare implements Comparator<Movie1>{
	public int compare(Movie1 m1, Movie1 m2) {
		return m1.getName().compareTo(m2.getName());
	}
}

public class comparatorClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Movie1> list = new ArrayList<Movie1>();
		list.add(new Movie1("Transformer", 8.8, 2014));
		list.add(new Movie1("X men", 8.4, 2019));
		list.add(new Movie1("Deadpool", 9.0, 2018));
		
		System.out.println("sorted by name : ");
		NameCompare nc = new NameCompare();
		Collections.sort(list, nc);
		for(Movie1 mov: list) {
			System.out.println(mov.getName() + " " + 
								mov.getRate() + " " + 
								mov.getYear());
		}
		
		System.out.println("\n\nsorted by rate : ");
		RateCompare rc = new RateCompare();
		Collections.sort(list, rc);
		for(Movie1 mov: list) {
			System.out.println(mov.getName() + " " + 
								mov.getRate() + " " + 
								mov.getYear());
		}
	}

}
