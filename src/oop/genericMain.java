package oop;

public class genericMain {

	public static <E> void print(E[] input) {
		for(E el : input) {
			System.out.printf("%s ", el);
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer[] intAr = {1,2,3};
		Double[] doubAr = {1.1,2.2,3.3};
		
		print(intAr);
		print(doubAr);
	}

}
